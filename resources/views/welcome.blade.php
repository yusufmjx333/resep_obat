@extends('layouts.layout_dasar')
@section('header')
<link rel="stylesheet" href="{{asset ('admin/assets/bundles/datatables/datatables.min.css')}}">
<link rel="stylesheet" href="{{asset ('admin/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset ('admin/assets/bundles/select2/dist/css/select2.min.css')}}">
 <link rel="stylesheet" href="{{asset ('admin/assets/bundles/izitoast/css/iziToast.min.css')}}">
<style type="text/css">
  .noround{
    border-radius: 0px;
  }
  .zero-p{
    padding: 0px;
    margin: 0px;
  }
  .styling-card{
    margin-top:-40px; margin-left: -15px;margin-right: -17px;
  }
</style>

@endsection
@section('main_content')
  
  <div class="row zero-p">
    <div class="col-12 zero-p">
      <div class="card author-box noround card-primary styling-card">
        <div class="card-header " style="font-size: 1.5em;">Pembuatan Resep</div>
        <div class="card-body zero-p" >
          <div class="row">
            <div class="col-12">
              <a class="btn btn-sm btn-success text-white" data-target="#resepModal" data-toggle="modal" style="text-transform: uppercase;"> <i class="fa fa-plus"></i> Catat Resep Baru</a>
            </div>
          </div>
          <hr>
          <div class="table-responsive">
            
            <table class="table table-sm table-bordered table-hover table-striped" id="table-1">
              <thead>
                <tr>
                  <th style="width:5%;">No</th>
                  <th style="width:20%;">Kode</th>
                  <th style="width:30%;">Nama</th>
                  <th style="width:40%;">Keterangan</th>
                  <th style="width:5%;">Detail</th>
               
                </tr>
              </thead>
              <tbody>
              
              </tbody>


            </table>

          </div>



        </div>
      </div>
    </div>
  </div>




@endsection
@section('js')


    <div class="modal fade bd-example-modal-lg" id="resepModal" role="dialog" aria-labelledby="myLargeModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="myLargeModalLabel">Pencatatan Resep</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div class="form-group">
                <div class="row">
                  <div class="col-md-6">
                    <input placeholder="Kode Resep" type="text" name="kode_resep" id="kode_resep" class="form-control">
                  </div>
                  <div class="col-md-6">
                    <input placeholder="Nama Resep" type="text" name="nama_resep" id="nama_resep" class="form-control">
                  </div>
                  <div class="col-md-12">
                  <hr>

                      <textarea placeholder="Keterangan" id="ket_resep" class="form-control"></textarea>
                  </div>
                </div>
                <hr>  
                <div class="row">
                  <div class="col-md-12 text-center">
                    <a class="btn btn-success btn-sm text-white" id="saveHead"> Tambah Detail </a>
                  </div>
                </div>
              </div>
           
              <div class="form-group" id="bentuk1_resep">
              
              </div>



              <div id="tampil_res">
                
              </div>
              <hr>

                 <div id="detail_table" class="table-responsive">
                <table class="table table-sm table-bordered table-hover table-striped" id="dtTable">
                  <thead>
                    <tr>
                      <th>Obat</th>
                      <th>Nama Obat</th>
                      <th>Jenis Obat</th>
                      <th>Jml</th>
                      <th>Signa</th>
                      <th>Keterangan</th>
                      <th><i class="fa fa-info"></i></th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </div>
           
              <button type="button" class="btn btn-primary m-t-15 waves-effect" id="end">Selesai</button>
              </div>
            </div>
          </div>
        </div>




  <script src="{{asset ('admin/assets/bundles/datatables/datatables.min.js')}}"></script>
  <script src="{{asset ('admin/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset ('admin/assets/bundles/select2/dist/js/select2.full.min.js')}}"></script>

  <script src="{{asset ('admin/assets/bundles/izitoast/js/iziToast.min.js')}}"></script>
  <script type="text/javascript">

     function detailT(){

        $('#tambahDetail').click(function(){


          var obat_resep = $('#obat_resep').val();
          var kode_resep = $('#kode_resep').val();
          var signa_resep = $('#signa_resep').val();
          var jml_obat = $('#jml_obat').val();
          var ket_obat = $('#ket_obat').val();
          var bentuk_resep = $('#bentuk_resep').val();
          var namaObat = $('#namaObat').val();


            $.ajax({
                type: 'GET',
                url: '/tambah/detail/resep',
                data: {
                  'kode_resep': kode_resep,
                  'obat_resep': obat_resep,
                  'jenis_obat': bentuk_resep ,
                  'signa_resep': signa_resep,
                  'jml_obat': jml_obat,
                  'ket_obat': ket_obat,
                  'namaObat': namaObat,
                  
                
                },
                success:function(data){
                  if((data.errors)){
                  }else{
                    if (data = "success") {

                      createTab2($('#kode_resep').val());
                        // $('#obat_resep').val('');
                        // $('#signa_resep').val('');
                        // $('#jml_obat').val('');
                        // $('#ket_obat').val('');
                        // $('#namaObat').val('');
                        $('#tampil_res').empty();

                    }
                  }
                }
              })




        })

      }

     function detailT2(){

        $('#tambahDetail2').click(function(){


          var obat_resep = $('#obatMulti').val();
          var kode_resep = $('#kode_resep').val();
          var signa_resep = $('#signa_resep').val();
          var jml_obat = $('#jml_obat').val();
          var ket_obat = $('#ket_obat').val();
          var bentuk_resep = $('#bentuk_resep').val();
          var namaObat = $('#namaObat').val();


            $.ajax({
                type: 'GET',
                url: '/tambah/detail/resep',
                data: {
                  'kode_resep': kode_resep,
                  'obat_resep': obat_resep,
                  'jenis_obat': bentuk_resep ,
                  'signa_resep': signa_resep,
                  'jml_obat': jml_obat,
                  'ket_obat': ket_obat,
                  'namaObat': namaObat,
                  
                
                },
                success:function(data){
                  if((data.errors)){
                  }else{
                    if (data = "success") {

                      createTab2($('#kode_resep').val());
                        // $('#obat_resep').val('');
                        // $('#signa_resep').val('');
                        // $('#jml_obat').val('');
                        // $('#ket_obat').val('');
                        // $('#namaObat').val('');
                        $('#tampil_res').empty();

                    }
                  }
                }
              })




        })

      }




    // table
    // $("#dtTable").dataTable();
    createTab();
    function createTab(){

    $('#table-1').DataTable().destroy();
      $('#table-1').DataTable({
            processing: true,
            // serverSide: true,
                  stateSave: true,
                  autoWidth: false,
            ajax:{
              "url": '/tampil/resep',
              "dataSrc": ""
            } ,

            columnDefs: [
              {
                  targets: [0,4],
                  createdCell: function(td){
                    $(td).css('text-align', 'center');
                    $(td).css('padding', '3px');
                  }
              },
              {
                  targets: [1,2,3],
                  createdCell: function(td){
                    // $(td).css('text-align', 'center');
                    $(td).css('padding', '3px');
                  }
              },
              
            ],
            
            columns: [
               
                { "data": null, 
                     render:  function (data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                             
                            }  
                  },
                {"data":"kode_resep"},
                {"data":"nama_resep"},
                {"data":"keterangan"},
                {"data":null, 
                  render:function (data, type, row, meta) {

                    return '<a class="text-info"><i class="fa fa-eye"></i>';

                  }

                },
             
            ]
        });


      }

      function createTab2(kode){

    $('#dtTable').DataTable().destroy();
      $('#dtTable').DataTable({
            processing: true,
            // serverSide: true,
                  stateSave: true,
                  autoWidth: false,
            ajax:{
              "url": '/tampil/detail/resep/'+kode,
              "dataSrc": ""
            } ,

            columnDefs: [
              {
                  targets: [4],
                  createdCell: function(td){
                    $(td).css('text-align', 'center');
                    $(td).css('padding', '3px');
                  }
              },
              {
                  targets: [0,1,2,3],
                  createdCell: function(td){
                    // $(td).css('text-align', 'center');
                    $(td).css('padding', '3px');
                  }
              },
              
            ],
            
            columns: [
               
                // { "data": null, 
                //      render:  function (data, type, row, meta) {
                //               return meta.row + meta.settings._iDisplayStart + 1;
                             
                //             }  
                //   },
                {"data":"gabung_obat"},
                {"data":"nama_obat"},
                {"data":"jns"},
                {"data":"jml"},
                {"data":"signa"},
                {"data":"ket"},
                {"data":null, 
                  render:function (data, type, row, meta) {

                    return '<a class="text-danger"><i class="fa fa-times"></i>';

                  }

                },
             
            ]
        });


      }






     $('#saveHead').click(function(){

        var kode = $('#kode_resep').val();
        var nama = $('#nama_resep').val();
        var ket = $('#ket_resep').val();



        $.ajax({
            type: 'GET',
            url: '/tambah/kepala/obat',
            data: {
              'kode': kode,
              'nama': nama,
              'ket': ket,
              
            
            },
            success:function(data){
              if((data.errors)){
              }else{

                if (data == 'success') {
                  createTab();
                 iziToast.success({
                    title: 'Berhasil !',
                    message: 'Pendaftaran Kode dan Nama resep berhasil ',
                    position: 'topRight'
                  });
                   document.getElementById("kode_resep").readOnly = true; 
                  document.getElementById("nama_resep").readOnly = true; 
                  document.getElementById("ket_resep").readOnly = true; 


                  $('#bentuk1_resep').empty();
                  $('#bentuk1_resep').append(
                    '<label style="font-weight: bold;">Bentuk Resep</label>'+
                '<div class="input-group">'+
                  '<div class="input-group-prepend">'+
                    '<div class="input-group-text">'+
                      '<i class="fas fa-list"></i>'+
                    '</div>'+
                  '</div>'+
                  '<select class="form-control" id="bentuk_resep" name="bentuk_resep">'+
                    
                    '<option value="0" selected="true">Pilih</option>'+
                    '<option value="1" >Racikan</option>'+
                    '<option value="2" >Non Racikan</option>'+
                  '</select>'+
                '</div>');

                  $('#bentuk_resep').on('change', function(){

                    $('#tampil_res').empty();
                    if ($('#bentuk_resep').val() == 1) {
                      $('#tampil_res').append('<div class="form-group">'+
                              '<label style="font-weight: bold;">Obat</label>'+
                              '<div class="input-group">'+
                               
                               ' <select class="form-control selecting" id="obat_resep" name="obat_resep" style="width:100%;">'+
                                 ' <option value="0" selected="true"> <font color="gray">Pilih obat</font></option>'+
                                
                               ' </select>'+
                             ' </div>'+
                           ' </div>'+

                           ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Nama Obat</label>'+
                             ' <div class="input-group">'+
                               
                               ' <input type="text" class="form-control" id="namaObat">'+
                             ' </div>'+
                           ' </div>'+

                             ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Signa</label>'+
                             ' <div class="input-group">'+
                               
                               ' <select class="form-control selecting" id="signa_resep" name="signa_resep" style="width:100%;">'+
                                 ' <option value="0" disabled="true" selected="true"> <font color="gray">Pilih Signa</font></option>'+
                                
                               ' </select>'+
                             ' </div>'+
                           ' </div>'+



                            ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Jml Obat (Jml Stock : <a style="font-weight:bold;"> <i id="tampilan_jml_stock">0</i> </a> )</label>'+
                             ' <div class="input-group">'+
                               ' <div class="input-group-prepend">'+
                                 ' <div class="input-group-text">'+
                                   ' <i class="fas fa-asterisk"></i>'+
                                 ' </div>'+
                               ' </div>'+
                               ' <input type="hidden" name="jml_stock_obat" id="jml_stock_obat">'+
                               ' <input type="number" name="jml_obat" id="jml_obat" class="form-control">'+
                             ' </div>'+

                               ' <code id="alertJml">'+
                                 
                               ' </code>'+

                            ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Keterangan</label>'+
                             ' <div class="input-group">'+
                               ' <div class="input-group-prepend">'+
                                 ' <div class="input-group-text">'+
                                   ' <i class="fas fa-asterisk"></i>'+
                                 ' </div>'+
                               ' </div>'+
                               ' <input type="text" name="ket_obat" id="ket_obat" class="form-control">'+
                             ' </div>'+
                           ' </div><hr><div class="row "><div class="col text-right"><a class="btn btn-sm btn-success text-white" id="tambahDetail">TAMBAH DETAIL</a></div></div>'
                        )


                      tarik();
                        $('.selecting').select2();
                        detailT();
                    }else if($('#bentuk_resep').val() == 2){

                       $('#tampil_res').append(



                            '<div class="form-group">'+
                              '<label>Obat</label>'+
                               '<select class="form-control selecting" id="obatMulti" multiple=""  style="width:100%;"></select>'+
                                   
                                  
                                
                            '</div>'+


                           ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Nama Obat</label>'+
                             ' <div class="input-group">'+
                               
                               ' <input type="text" class="form-control" id="namaObat">'+
                             ' </div>'+
                           ' </div>'+

                             ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Signa</label>'+
                             ' <div class="input-group">'+
                               
                               ' <select class="form-control selecting" id="signa_resep" name="signa_resep" style="width:100%;">'+
                                 ' <option value="0" disabled="true" selected="true"> <font color="gray">Pilih Signa</font></option>'+
                                
                               ' </select>'+
                             ' </div>'+
                           ' </div>'+



                            ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Jml Obat </label>'+
                             ' <div class="input-group">'+
                               ' <div class="input-group-prepend">'+
                                 ' <div class="input-group-text">'+
                                   ' <i class="fas fa-asterisk"></i>'+
                                 ' </div>'+
                               ' </div>'+
                               ' <input type="hidden" name="jml_stock_obat" id="jml_stock_obat">'+
                               ' <input type="number" name="jml_obat" id="jml_obat" class="form-control">'+
                             ' </div>'+

                               ' <code id="alertJml">'+
                                 
                               ' </code>'+

                            ' <div class="form-group">'+
                             ' <label style="font-weight: bold;">Keterangan</label>'+
                             ' <div class="input-group">'+
                               ' <div class="input-group-prepend">'+
                                 ' <div class="input-group-text">'+
                                   ' <i class="fas fa-asterisk"></i>'+
                                 ' </div>'+
                               ' </div>'+
                               ' <input type="text" name="ket_obat" id="ket_obat" class="form-control">'+
                             ' </div>'+
                           ' </div><hr><div class="row "><div class="col text-right"><a class="btn btn-sm btn-success text-white" id="tambahDetail2">TAMBAH DETAIL</a></div></div>'
                        )


                      tarik2();
                        $('.selecting').select2();
                        detailT2();

                    }else{

                    }


                  })

                  $('#saveHead').hide();
                  $('#end').show();

                }else if(data == 'warning'){
                      iziToast.warning({
                        title: 'Kode atau Nama Kosong  !',
                        message: 'Harap untuk masukan kode dan nama diisi ',
                        position: 'topRight'
                      });
                }
                else{

                      iziToast.error({
                        title: 'Gagal Kode Sudah terpakai !',
                        message: 'Gagal melakukan pendaftaran  Kode dan Nama resep',
                        position: 'topRight'
                      });
                }


                // reloading();
              
              } 
        
            },
          });

     })
  


  function tarik(){

    // pilihan obat
    $.get('/pilihan/obat', function(data){
      $('#obat_resep').empty();
      $('#obat_resep').append(' <option value="0"  selected="true"> <font color="gray">Pilih obat</font></option>');

      $.each(data, function(index, dataObj){
        $('#obat_resep').append("<option value='"+dataObj.obatalkes_kode+"'>"+dataObj.obatalkes_nama+" - "+dataObj.obatalkes_kode+"</option>");
      })

    })
    // Pilihan Signa
     $.get('/pilihan/signa', function(data){
      $('#signa_resep').empty();
      $('#signa_resep').append(' <option value="0" selected="true"> <font color="gray">Pilih signa</font></option>');

      $.each(data, function(index, dataObj){
        $('#signa_resep').append("<option value='"+dataObj.signa_kode+"'>"+dataObj.signa_nama+" - "+dataObj.signa_kode+"</option>");
      })

    })

    $('#obat_resep').on('change', function(){
       // Ambil Jml Stock Obat
       $.get('/lihat/jmlObat/'+$('#obat_resep').val(), function(data){
        $('#jml_stock_obat').val('');
        $('#jml_stock_obat').val(data);
        $('#tampilan_jml_stock').empty();
        $('#tampilan_jml_stock').text(data);
      })

       $.get('/lihat/namaObat/'+$('#obat_resep').val(), function(data){
        $('#namaObat').val('');
       
        $('#namaObat').val(data);
      })




    })


     $('#jml_obat').keyup(function(){
        var jml_obat = Number($('#jml_obat').val());
        var jml_stock_obat = Number($('#jml_stock_obat').val());

        if(jml_obat > jml_stock_obat){
          $('#alertJml').empty()
          $('#alertJml').append(' Jumlah obat melebihi stock !')
        }else{
          $('#alertJml').empty()
          $('#alertJml').append('<i class="fa fa-check"></i>')
        }


     })
  }
   


   function tarik2(){

  $.get('/pilihan/obat', function(data){
      $('#obatMulti').empty();
      // $('#obatMulti').append(' <option value="0"  selected="true"> <font color="gray">Pilih obat</font></option>');

      $.each(data, function(index, dataObj){
        $('#obatMulti').append("<option value='"+dataObj.obatalkes_kode+"'>"+dataObj.obatalkes_nama+" - "+dataObj.obatalkes_kode+"</option>");
      })

    })
  
    // pilihan obat
    // $.get('/pilihan/obat', function(data){
    //   $('#obat_resep').empty();
    //   // $('#obat_resep').append(' <option value="0"  selected="true"> <font color="gray">Pilih obat</font></option>');

    //   $.each(data, function(index, dataObj){
    //     $('#obat_resep').append("<option value='"+dataObj.obatalkes_kode+"'>"+dataObj.obatalkes_nama+" - "+dataObj.obatalkes_kode+"</option>");
    //   })

    // })
    // Pilihan Signa
     $.get('/pilihan/signa', function(data){
      $('#signa_resep').empty();
      $('#signa_resep').append(' <option value="0" selected="true"> <font color="gray">Pilih signa</font></option>');

      $.each(data, function(index, dataObj){
        $('#signa_resep').append("<option value='"+dataObj.signa_kode+"'>"+dataObj.signa_nama+" - "+dataObj.signa_kode+"</option>");
      })

    })

    $('#obatMulti').on('change', function(){
       // Ambil Jml Stock Obat
      //  $.get('/lihat/jmlObat/'+$('#obat_resep').val(), function(data){
      //   $('#jml_stock_obat').val('');
      //   $('#jml_stock_obat').val(data);
      //   $('#tampilan_jml_stock').empty();
      //   $('#tampilan_jml_stock').text(data);
      // })

      //  $.get('/lihat/namaObat/'+$('#obat_resep').val(), function(data){
      //   $('#namaObat').val('');
       
      //   $('#namaObat').val(data);
      // })

      console.log($('#obatMulti').val());


    })


     // $('#jml_obat').keyup(function(){
     //    var jml_obat = Number($('#jml_obat').val());
     //    var jml_stock_obat = Number($('#jml_stock_obat').val());

     //    if(jml_obat > jml_stock_obat){
     //      $('#alertJml').empty()
     //      $('#alertJml').append(' Jumlah obat melebihi stock !')
     //    }else{
     //      $('#alertJml').empty()
     //      $('#alertJml').append('<i class="fa fa-check"></i>')
     //    }


     // })
  }
  </script>





    <script type="text/javascript">
      
    $(document).ready(function() {
        $('.selecting').select2();

        $('#resepModal').on('hide.bs.modal', function () {
            document.getElementById("kode_resep").readOnly = false; 
            document.getElementById("nama_resep").readOnly = false; 
            document.getElementById("ket_resep").readOnly = false; 
            $('#kode_resep').val('');
            $('#nama_resep').val('');
            $('#ket_resep').val('');
        });

        $('#resepModal').on('show.bs.modal', function () {
            $('#end').hide();
        });
       
        $('#end').click(function(){
          var isi = $('#kode_resep').val();

        $.ajax({
            type: 'GET',
            url: '/update/kepala/obat',
            data: {
              'kode': isi,
              
              
              
            
            },
            success:function(data){
              if((data.errors)){
              }else{

              }
            }
          })


        })



    });
    </script>
@endsection