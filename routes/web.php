<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\obatalkesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [obatalkesController::class, 'index']);
Route::get('/pilihan/obat', [obatalkesController::class, 'daftarObat']);
Route::get('/pilihan/signa', [obatalkesController::class, 'daftarSigna']);
Route::get('/lihat/jmlObat/{id}', [obatalkesController::class, 'jmlObat']);
Route::get('/tambah/kepala/obat', [obatalkesController::class, 'kepalaObat']);
Route::get('/tampil/resep', [obatalkesController::class, 'tampilResep']);
Route::get('/tampil/detail/resep/{kode}', [obatalkesController::class, 'tampilDetailResep']);
Route::get('/tambah/detail/resep', [obatalkesController::class, 'detailObat']);
Route::get('/lihat/namaObat/{id}', [obatalkesController::class, 'nmObat']);
Route::get('/update/kepala/obat', [obatalkesController::class, 'uPkepalaObat']);
