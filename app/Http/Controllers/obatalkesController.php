<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\obatalkes;
use App\Models\signa;
use App\Models\resep;
use App\Models\detailResep;
use Illuminate\Support\Facades\DB;


class obatalkesController extends Controller
{
    public function index(){

        $data = obatalkes::all();

        return view('welcome', compact('data'));

    }

    public function daftarObat(){

        $data = DB::table('obatalkes_m')->get();

        return response()->json($data);


    }
     public function daftarSigna(){

        $data = DB::table('signa_m')->get();

        return response()->json($data);


    }


     public function jmlObat($id){

        $data = DB::table('obatalkes_m')->where('obatalkes_kode', '=', $id)->value('stok');

        return response()->json($data);


    }


     public function nmObat($id){

        $data = DB::table('obatalkes_m')->where('obatalkes_kode', '=', $id)->value('obatalkes_nama');

        return response()->json($data);


    }

    public function kepalaObat(Request $request){

        $checker = DB::table('reseps')->where('kode_resep', '=', $request->kode)->count();

        if ($request->kode == null && $request->nama == null) {
            return "warning";
        }

        if ($checker > 0) {
            return "failed";
        }else{

            $resep = new resep;
            $resep->kode_resep = $request->kode;
            $resep->nama_resep = $request->nama;
            $resep->keterangan = $request->ket;
            // $resep->save();


            if ($resep->save()) {
                return "success"; 
            }else if(!$resep->save()){
                return "warning"; 
            }

        }



    }

    
    public function uPkepalaObat(Request $req){

               $checker = DB::table('reseps')->where('kode_resep', '=', $req->kode)->value('id');

        $resep = resep::findOrFail($checker);
        $resep->status = 'F';
        // $resep->save();


        if ($resep->save()) {
            return "success"; 
        }else {
            return "warning"; 
        }
    }

    public function tampilResep(){


        $resep = DB::table('reseps')->orderBy('id', 'DESC')->get();

        return response()->json($resep);


    }


    public function tampilDetailResep($kode){

        $detail = DB::table('detail_reseps')->where('kode_resep', '=', $kode)->get();

        $gabung_obat = [];
        $nama_obat = [];
        $jml = [];
        $signa = [];
        $ket = [];
        $jns = [];


        foreach($detail as $dt){
            $s = explode(';', $dt->obatalkes_kode);
            $c = count($s);
            $nmx = '';
            if ($c > 1) {
                for ($i=0; $i < $c - 1 ; $i++) { 
                    $nm = DB::table('obatalkes_m')->where('obatalkes_kode', '=', $s[$i])->value('obatalkes_nama');
                    $nmx .=  $nm .";";
                }
            }else{
                $nmx = DB::table('obatalkes_m')->where('obatalkes_kode', '=', $s[0])->value('obatalkes_nama');
            }

            $gabung_obat[] = $nmx;
            $nama_obat[] = $dt->nama_obat;
            $jml[] = $dt->Jml;
            $signa[] = DB::table('signa_m')->where('signa_kode', '=', $dt->signa_obat)->value('signa_nama');
            $ket[] = $dt->keterangan;
                if ($dt->jenis_obat == 1) {
                    $jns[] = "Racik";
                    
                }else if($dt->jenis_obat == 2){
                    $jns[] = "Non Racik";

                }



        }
     
        $cx = count($gabung_obat);
        $all = [];
        for ($i=0; $i < $cx ; $i++) { 
            $all[] = array(
                "gabung_obat" => $gabung_obat[$i],
                "nama_obat" => $nama_obat[$i],
                "jml" => $jml[$i],
                "signa" => $signa[$i],
                "ket" => $ket[$i],
                "jns" => $jns[$i],
            );
        }

        return response()->json($all);

    }


    public function detailObat(Request $req){
        $c = 0;

            if (is_array($req->obat_resep)) {
               $c = count($req->obat_resep);
            }
        
        if ($c > 0) {

            $obt = '';
            for ($i=0; $i <$c ; $i++) { 
                $obt .= $req->obat_resep[$i].';';
            }

            $detail = new detailResep;
            $detail->kode_resep = $req->kode_resep;
            $detail->obatalkes_kode = $obt;
            $detail->signa_obat = $req->signa_resep;
            $detail->Jml = $req->jml_obat;
            $detail->jenis_obat = $req->jenis_obat;
            $detail->keterangan = $req->ket_obat;
            $detail->nama_obat = $req->namaObat;
            $detail->save();


        }else{
            $detail = new detailResep;
            $detail->kode_resep = $req->kode_resep;
            $detail->obatalkes_kode = $req->obat_resep;
            $detail->signa_obat = $req->signa_resep;
            $detail->Jml = $req->jml_obat;
            $detail->jenis_obat = $req->jenis_obat;
            $detail->keterangan = $req->ket_obat;
            $detail->nama_obat = $req->namaObat;
            $detail->save();

        }


        return "success";


    }
}
