<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class detailResep extends Model
{
    protected $fillable = [
        'id',
        'kode_resep',
        'obatalkes_kode',
        'jml',
        'keterangan',
        'jenis_obat',
        'nama_obat',
        'signa_obat',


    ];
}
